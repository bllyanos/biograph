import React from 'react';
import './index.css';
const App = () => {
  return (
    <div className="app">
      <div className="jumbotron-mask">
        <div className="jumbotron jumbotron-fluid gradient-jumbotron">
          <div className="container text-white text-right">
            <h1 className="display-4 jomol font-weight-bold"><mark>Billy Editiano</mark></h1>
            <p className="lead"><mark>Web Developer.</mark></p>
          </div>
        </div>
      </div>


      <div className="container pt-1 pb-5">
        <div className="row justify-content-center">
          <div className="col-10 col-md-6">
            <div className="card text-center text-dark squared">
              <div className="card-header">
                Featured Project
              </div>
              <div className="card-body jumbo-image" style={{ height: '200px' }}>

              </div>
              <div className="card-body">
                <h5 className="card-title">Bitbull</h5>
                <p className="card-text">Full-featured card game, written in NodeJS, uses WebSocket, backed by AWS-Lambda and Serverless Technology.</p>
                <a href="#" className="btn btn-primary squared">Learn More</a>
              </div>
              <div className="card-footer text-muted">
                2 days ago
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;